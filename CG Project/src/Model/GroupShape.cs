﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Draw.src.Model
{
    [Serializable]

    public class GroupShape : Shape
    {
        #region Constructor

        public GroupShape(RectangleF rect)
            : base(rect)
        {
            SubItems = new List<Shape>();
        }

        public GroupShape()
        {
            SubItems = new List<Shape>();
        }

        #endregion

        public List<Shape> SubItems { get; set; }

        public PointF PointToRotateAround { get; set; }
        
        public override void DrawSelf(Graphics grfx)
        {
            Point point = new Point((int)(Location.X + Width / 2), (int)(Location.Y + Height / 2));
            base.RotateShape(grfx, PointToRotateAround);

            base.DrawSelf(grfx);

            grfx.ResetTransform();

            foreach (var item in SubItems)
            {
                base.RotateShape(grfx);

                item.DrawSelf(grfx);
                grfx.ResetTransform();
            }
            
        }

        public override void Move(float dx, float dy)
        {
            base.Move(dx, dy);
            foreach (var item in SubItems)
            {
                item.Move(dx, dy);
            }
        }

        public void GroupRotate(GroupShape shape, PointF point)
        {
            foreach (var item in shape.SubItems)
            {
                if ((item is GroupShape g) && g.SubItems.Any(x => x.GetType() == typeof(GroupShape)))
                {
                    GroupRotate(g, point);
                    item.CurrentAngle += 90;
                    g.PointToRotateAround = point;
                }
                else if (item is GroupShape gr)
                {
                    item.CurrentAngle += 90;
                    gr.PointToRotateAround = point;

                }
                else PointToRotateAround = point;
            }
        }

        public override void ZoomInShape()
        {
            base.ZoomInShape();
            foreach (var item in SubItems)
            {
                item.ZoomInShape();
            }
        }

        public override void ZoomOutShape()
        {
            base.ZoomOutShape();
            foreach (var item in SubItems)
            {
                item.ZoomOutShape();
            }
        }

        public override Color FillColor
        {
            set
            {
                foreach (var item in SubItems)
                {
                    item.FillColor = value;
                }
            }
        }
        public override Color BorderColor
        {
            set
            {
                foreach (var item in SubItems)
                {
                    item.BorderColor = value;
                }
            }
        }
        public override float BorderSize
        {
            set
            {
                foreach (var item in SubItems)
                {
                    item.BorderSize = value;
                }
            }
        }

    }
}
