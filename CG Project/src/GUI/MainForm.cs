﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Draw
{
    /// Върху главната форма е поставен потребителски контрол, в който се осъществява визуализацията
    public partial class MainForm : Form
    {
        private DialogProcessor dialogProcessor = new DialogProcessor();
        bool HaveToRemoveElement, isCtrlPressed;
        Shape selectedElement;

        /// Агрегирания диалогов процесор във формата улеснява манипулацията на модела.
        public MainForm()
        {
            // The InitializeComponent() call is required for Windows Forms designer support.
            InitializeComponent();
            // TODO: Add constructor code after the InitializeComponent() call.
        }

        /// Събитието, което се прихваща, за да се превизуализира при изменение на модела.
        void ViewPortPaint(object sender, PaintEventArgs e)
        {
            dialogProcessor.ReDraw(sender, e);
        }

        #region ToolStripMenu          
        private void OpenToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.OpenFile(openFileDialog.FileName);
                viewPort.Invalidate();
            }
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.SaveAs(saveFileDialog.FileName);
            }
        }
        private void copyItem_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.Selection != null)
            {
                Clipboard.SetData("Selection", dialogProcessor.Selection);
            }
        }

        private void pasteItem_Click(object sender, EventArgs e)
        {
            if (Clipboard.ContainsData("Selection"))
            {
                dialogProcessor.Selection = (List<Shape>)Clipboard.GetData("Selection");
                dialogProcessor.Selection.ForEach(x => x.Move(20, 20));
                foreach (var item in dialogProcessor.Selection)
                {
                    dialogProcessor.ShapeList.Add(item);
                }
            }
            viewPort.Invalidate();
        }

        private void deleteItem_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.Selection != null)
            {
                dialogProcessor.ShapeList.RemoveAll(x => dialogProcessor.Selection.Contains(x));
                dialogProcessor.Selection.Clear();
                viewPort.Invalidate();
            }
        }

        /// Изход от програмата. Затваря главната форма, а с това и програмата.        
        void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }
        #endregion ToolStripMenu

        #region Buttons

        /// Бутон, който поставя на произволно място правоъгълник със зададените размери.
        /// Променя се лентата със състоянието и се инвалидира контрола, в който визуализираме.
        void DrawRectangleSpeedButtonClick(object sender, EventArgs e)
        {
            dialogProcessor.AddRandomRectangle();

            statusBar.Items[0].Text = "Последно действие: Рисуване на правоъгълник";

            viewPort.Invalidate();
        }

        void DrawEllipseButton(object sender, EventArgs e)
        {
            dialogProcessor.AddRandomEllipse();

            statusBar.Items[0].Text = "Последно действие: Рисуване на елипса";

            viewPort.Invalidate();
        }

        private void DrawTriangleButton_Click(object sender, EventArgs e)
        {
            dialogProcessor.AddRandomTriangle();

            statusBar.Items[0].Text = "Последно действие: Рисуване на триъгълник";

            viewPort.Invalidate();
        }

        void DrawCrossedLinesButtonClick(object sender, EventArgs e)
        {
            dialogProcessor.AddRandomCrossedLines();

            statusBar.Items[0].Text = "Последно действие: Рисуване на пресечни линии";

            viewPort.Invalidate();
        }

        void FillColorClick(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.SetFillColor(colorDialog.Color);
                statusBar.Items[0].Text = "Последно действие: Запълване с цвят";
                viewPort.Invalidate();
            }
        }

        void BorderColorClick(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                dialogProcessor.SetBorderColor(colorDialog.Color);
                statusBar.Items[0].Text = "Последно действие: Очертаване с цвят";
                viewPort.Invalidate();
            }
        }

        private void GroupButtonClick(object sender, EventArgs e)
        {
            dialogProcessor.GroupSelected();
            viewPort.Invalidate();
        }

        private void SeparateButtonClick(object sender, EventArgs e)
        {
            if (dialogProcessor.Selection.Exists(x => x.GetType() == dialogProcessor.group.GetType()))
                dialogProcessor.SeparateSelected();
            viewPort.Invalidate();
        }

        private void RotateShapeButton_Click(object sender, EventArgs e)
        {
            dialogProcessor.Rotate();
            viewPort.Invalidate();
        }

        private void borderSize2_Click(object sender, EventArgs e)
        {
            dialogProcessor.SetBorderSize(2);
            statusBar.Items[0].Text = "Последно действие: Задаване на размер на линия: 2";
            viewPort.Invalidate();
        }

        private void borderSize4_Click(object sender, EventArgs e)
        {
            dialogProcessor.SetBorderSize(4);
            statusBar.Items[0].Text = "Последно действие: Задаване на размер на линия: 4";
            viewPort.Invalidate();
        }

        private void borderSize6_Click(object sender, EventArgs e)
        {
            dialogProcessor.SetBorderSize(6);
            statusBar.Items[0].Text = "Последно действие: Задаване на размер на линия: 6";
            viewPort.Invalidate();
        }

        private void zoomIn_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.Selection != null)
            {
                foreach (var item in dialogProcessor.Selection)
                {
                    item.ZoomInShape();
                }
                viewPort.Invalidate();
            }
        }
        #endregion Buttons

        #region MouseEvents

        /// Прихващане на координатите при натискането на бутон на мишката и проверка (в обратен ред) дали не е щракнато върху елемент. Ако е така то той се отбелязва като селектиран и започва процес на "влачене".
        /// Промяна на статуса и инвалидиране на контрола, в който визуализираме. Реализацията се диалогът с потребителя, при който се избира "най-горния" елемент от екрана.
        void ViewPortMouseDown(object sender, MouseEventArgs m)
        {
            selectedElement = dialogProcessor.GetSelectedElementFromMouseClick(m.Location);
            if (selectedElement == null && ModifierKeys != Keys.Control)
                dialogProcessor.Selection.Clear();

            else if (selectedElement != null && dialogProcessor.Selection.Contains(selectedElement))
            {
                HaveToRemoveElement = true;
                isCtrlPressed = (ModifierKeys == Keys.Control);
            }
            else if (selectedElement != null && !dialogProcessor.Selection.Contains(selectedElement))
            {
                if (ModifierKeys == Keys.Control)
                {
                    dialogProcessor.Selection.Add(selectedElement);
                }
                else dialogProcessor.Selection = new List<Shape> { selectedElement };
            }

            if (dialogProcessor.Selection != null)
            {
                statusBar.Items[0].Text = "Последно действие: Селекция на примитив";
                dialogProcessor.IsDragging = true;
                dialogProcessor.LastLocation = m.Location;
                viewPort.Invalidate();
            }
        }

        /// Прихващане на преместването на мишката.
        /// Ако сме в режм на "влачене", то избрания елемент се транслира.
        void ViewPortMouseMove(object sender, MouseEventArgs e)
        {
            if (dialogProcessor.IsDragging)
            {
                if (dialogProcessor.Selection != null) statusBar.Items[0].Text = "Последно действие: Влачене";
                dialogProcessor.TranslateTo(e.Location);
                HaveToRemoveElement = false;
                viewPort.Invalidate();
            }
        }

        private void zoomOut_Click(object sender, EventArgs e)
        {
            if (dialogProcessor.Selection != null)
            {
                foreach (var item in dialogProcessor.Selection)
                {
                    item.ZoomOutShape();
                }
                viewPort.Invalidate();
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dialogProcessor.Selection.Clear();
            dialogProcessor.Selection.AddRange(dialogProcessor.ShapeList);
            viewPort.Invalidate();
        }

        private void SeparateAll_Click(object sender, EventArgs e)
        {
            dialogProcessor.SeparateAll();
            viewPort.Invalidate();
        }





        /// Прихващане на отпускането на бутона на мишката. Излизаме от режим "влачене".
        void ViewPortMouseUp(object sender, MouseEventArgs m)
        {
            dialogProcessor.IsDragging = false;
            if (HaveToRemoveElement)
            {
                if (isCtrlPressed)
                {
                    dialogProcessor.Selection.Remove(selectedElement);
                }
                else dialogProcessor.Selection = new List<Shape> { selectedElement };
                HaveToRemoveElement = false;
            }
            isCtrlPressed = false;
            viewPort.Invalidate();
        }
        #endregion MouseEvents
    }
}
