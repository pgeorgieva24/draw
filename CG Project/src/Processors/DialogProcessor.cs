﻿using Draw.src.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace Draw
{
    /// Класът, който ще бъде използван при управляване на диалога.
    public class DialogProcessor : DisplayProcessor
    {
        #region Constructor
        public DialogProcessor() { }
        #endregion

        #region Properties

        public GroupShape group = new GroupShape();

        /// Избран елемент.
        private List<Shape> selection = new List<Shape>();
        public List<Shape> Selection
        {
            get { return selection; }
            set { selection = value; }
        }

        /// Дали в момента диалога е в състояние на "влачене" на избрания елемент.
        private bool isDragging;
        public bool IsDragging
        {
            get { return isDragging; }
            set { isDragging = value; }
        }

        /// Последна позиция на мишката при "влачене". Използва се за определяне на вектора на транслация.
        private PointF lastLocation;
        public PointF LastLocation
        {
            get { return lastLocation; }
            set { lastLocation = value; }
        }
        #endregion

        #region Add Shape

        /// Добавя примитив - правоъгълник на произволно място върху клиентската област.

        public void AddRandomRectangle()
        {
            Random rnd = new Random();
            int x = rnd.Next(20, 1500);
            int y = rnd.Next(30, 550);

            RectangleShape rect = new RectangleShape(new Rectangle(x, y, 100, 200));
            rect.FillColor = Color.White;
            rect.BorderColor = Color.Black;

            ShapeList.Add(rect);
        }

        public void AddRandomEllipse()
        {
            Random rnd = new Random();
            int x = rnd.Next(20, 1400);
            int y = rnd.Next(30, 650);

            ElipseShape ellipse = new ElipseShape(new Rectangle(x, y, 200, 100));
            ellipse.FillColor = Color.White;
            ellipse.BorderColor = Color.Black;
            ShapeList.Add(ellipse);
        }

        public void AddRandomTriangle()
        {
            Random rnd = new Random();
            int x = rnd.Next(100, 1500);
            int y = rnd.Next(30, 600);
            //int z = x;
            Point point1 = new Point(x, y);
            Point point2 = new Point(x - 100, y + 150);
            Point point3 = new Point(x + 100, y + 150);
            PointF[] points =
                     {
                 point1,
                 point2,
                 point3
             };

            TriangleShape triangle = new TriangleShape(points);
            triangle.Height = 200;
            triangle.Width = 200;
            triangle.FillColor = Color.White;
            triangle.BorderColor = Color.Black;
            ShapeList.Add(triangle);
        }

        public void AddRandomCrossedLines()
        {
            Random rnd = new Random();
            int x = rnd.Next(20, 1500);
            int y = rnd.Next(30, 550);

            CrossedLinesShape crossedLines = new CrossedLinesShape(new Rectangle(x, y, 200, 100));
            crossedLines.FillColor = Color.White;
            crossedLines.BorderColor = Color.Black;

            ShapeList.Add(crossedLines);
        }
        #endregion Add Shapes

        /// Проверява дали дадена точка е в елемента и ако има такъв го връща.
        public Shape GetSelectedElementFromMouseClick(PointF point)
        {
            for (int i = ShapeList.Count - 1; i >= 0; i--)
            {
                if (ShapeList[i].ContainsPoint(point))
                {
                    return ShapeList[i];
                }
            }
            return null;
        }

        /// Транслация на избраният елемент на вектор определен от мишката.
        public void TranslateTo(PointF newPoind)
        {
            foreach (var item in Selection)
            {
                item.Move(newPoind.X - lastLocation.X, newPoind.Y - lastLocation.Y);
            }
            lastLocation = newPoind;
        }

        #region Change Colors

        public void SetFillColor(Color color)
        {
            foreach (var item in Selection)
            {
                item.FillColor = color;
            }
        }
        public void SetBorderColor(Color color)
        {
            foreach (var item in Selection)
            {
                item.BorderColor = color;
            }
        }

        public void SetBorderSize(float size)
        {
            foreach (var item in Selection)
            {
                item.BorderSize = size;
            }
        }
        #endregion

        public override void Draw(Graphics grfx)
        {
            base.Draw(grfx);
            foreach (var item in Selection)
            {
                item.RotateShape(grfx);
                grfx.DrawRectangle(Pens.Blue, item.Location.X - 2, item.Location.Y - 2, item.Width + 4, item.Height + 4);

                grfx.ResetTransform();
            }
        }

        public void GroupSelected()
        {
            if (Selection.Count < 2) return;
            float minX = float.PositiveInfinity, minY = float.PositiveInfinity, maxX = float.NegativeInfinity, maxY = float.NegativeInfinity;
            foreach (var item in Selection)
            {
                if (minX > item.Location.X) minX = item.Location.X;
                if (minY > item.Location.Y) minY = item.Location.Y;
                if (maxX < item.Location.X + item.Width) maxX = item.Location.X + item.Width;
                if (maxY < item.Location.Y + item.Height) maxY = item.Location.Y + item.Height;
                
            }

            group = new GroupShape(new RectangleF(minX, minY, maxX - minX, maxY - minY));


            group.SubItems.AddRange(Selection);
            foreach (var item in Selection)
            {
                ShapeList.Remove(item);
            }
            Selection = new List<Shape>();

            Selection.Add(group);
            ShapeList.Add(group);
        }

        public void SeparateSelected()
        {
            for (int i = ShapeList.Count - 1; i >= 0; i--)
            {
                if (ShapeList[i].GetType() == group.GetType())
                {
                    GroupShape groupshape = (GroupShape)ShapeList[i];
                    Selection.Remove(ShapeList[i]);
                    ShapeList.RemoveAt(i);
                    foreach (var item in groupshape.SubItems)
                    {
                        ShapeList.Add(item);
                    }
                    return;
                }
            }
        }

        public void SeparateAll()
        {

            for (int i = ShapeList.Count - 1; i >= 0; i--)
            {
                if (ShapeList[i] is GroupShape groupshape)
                {
                    Selection.Remove(ShapeList[i]);
                    ShapeList.RemoveAt(i);
                    foreach (var item in groupshape.SubItems)
                    {
                        ShapeList.Add(item);
                    }
                    SeparateAll();

                }
            }

        }

        public void Rotate()
        {
            if (Selection.Count != 0)
            {
                foreach (var item in Selection)
                {
                    if (item is GroupShape g)
                    {
                        Point point = new Point((int)(g.Location.X + g.Width / 2), (int)(g.Location.Y + g.Height / 2));
                        g.GroupRotate(g, point);
                        g.CurrentAngle += 90;
                    }
                    else
                    {
                        item.CurrentAngle = item.CurrentAngle + 90;
                        
                    }

                }
            }
        }

        public void OpenFile(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            ShapeList = (List<Shape>)bf.Deserialize(fs);
            fs.Close();
        }

        public void SaveAs(string fileName)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, ShapeList);
            fs.Close();
        }
    }
}
