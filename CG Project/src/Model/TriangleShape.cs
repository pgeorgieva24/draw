﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Draw.src.Model
{
    [Serializable]

    class TriangleShape : Shape
    {
        PointF[] points;
        #region Constructor
        PointF location;
        public override PointF Location
        {
            get => location;
            set => location = value;
        }

        float height;
        float width;
        public override float Height { set => height=value;  get => height; }
        public override float Width { set => width = value; get => width; }

        public TriangleShape(PointF[] points)
        {
            this.points = points;
            location = new PointF(points[1].X, points[0].Y);

        }

        #endregion

        float sign(PointF p1, PointF p2, PointF p3)
        {
            return (p1.X - p3.X) * (p2.Y - p3.Y) - (p2.X - p3.X) * (p1.Y - p3.Y);
        }

        bool PointInTriangle(PointF pt, PointF point1, PointF point2, PointF point3)
        {
            bool b1, b2, b3;

            b1 = sign(pt, point1, point2) < 0.0f;
            b2 = sign(pt, point2, point3) < 0.0f;
            b3 = sign(pt, point3, point1) < 0.0f;

            return ((b1 == b2) && (b2 == b3));
        }

        public override bool ContainsPoint(PointF point)
        {
            return PointInTriangle(point, points[0], points[1], points[2]);
        }
        public override void Move(float dx, float dy)
        {
            points[0].X = points[0].X + dx;
            points[0].Y = points[0].Y + dy;
            points[1].X = points[1].X + dx;
            points[1].Y = points[1].Y + dy;
            points[2].X = points[2].X + dx;
            points[2].Y = points[2].Y + dy;

            Location = new PointF(points[1].X, points[0].Y);

        }

        public override void RotateShape(Graphics grfx, PointF? point)
        {
            grfx.TranslateTransform(location.X + Width / 2, location.Y + Height / 2);
            grfx.RotateTransform(CurrentAngle);
            grfx.TranslateTransform(-(location.X + Width / 2), -(location.Y + Height / 2));
        }

        public override void ZoomInShape()
        {
            points[0].X = points[0].X ;
            points[0].Y = points[0].Y -10;
            points[1].X = points[1].X - 10;
            points[1].Y = points[1].Y + 10;
            points[2].X = points[2].X + 10;
            points[2].Y = points[2].Y + 10;

            Location = new PointF(points[1].X, points[0].Y);
            Height += 20;
            Width += 20;
        }

        public override void ZoomOutShape()
        {
            points[0].X = points[0].X;
            points[0].Y = points[0].Y + 10;
            points[1].X = points[1].X + 10;
            points[1].Y = points[1].Y - 10;
            points[2].X = points[2].X - 10;
            points[2].Y = points[2].Y - 10;

            Location = new PointF(points[1].X, points[0].Y);
            Height -= 20;
            Width -= 20;
        }

        public override void DrawSelf(Graphics grfx)
        {
            RotateShape(grfx, null);
            grfx.FillPolygon(new SolidBrush(FillColor), points);
            grfx.DrawPolygon(new Pen(BorderColor, BorderSize), points);
            grfx.ResetTransform();
        }
    }
}
