﻿using System;
using System.Drawing;

namespace Draw
{
    /// <summary>
    /// Базовия клас на примитивите, който съдържа общите характеристики на примитивите.
    /// </summary>
    [Serializable]
    public abstract class Shape
    {
        #region Constructors

        public Shape()
        {
        }

        public Shape(RectangleF rect)
        {
            rectangle = rect;
        }

        public Shape(Shape shape)
        {
            this.Height = shape.Height;
            this.Width = shape.Width;
            this.Location = shape.Location;
            this.rectangle = shape.rectangle;

            this.FillColor = shape.FillColor;
        }
        #endregion

        #region Properties

        private RectangleF rectangle;
        public virtual RectangleF Rectangle
        {
            get { return rectangle; }
            set { rectangle = value; }
        }

        public virtual float Width
        {
            get { return Rectangle.Width; }
            set { rectangle.Width = value; }
        }

        public virtual float Height
        {
            get { return Rectangle.Height; }
            set { rectangle.Height = value; }
        }
        public virtual PointF Location
        {
            get { return Rectangle.Location; }
            set { rectangle.Location = value; }
        }

        private Color fillColor;
        public virtual Color FillColor
        {
            get { return fillColor; }
            set { fillColor = value; }
        }

        private Color borderColor;
        public virtual Color BorderColor
        {
            get { return borderColor; }
            set { borderColor = value; }
        }

        private float currentAngle;
        public virtual float CurrentAngle
        {
            get { return currentAngle; }
            set { currentAngle = value; }
        }

        private float borderSize = 2;
        public virtual float BorderSize
        {
            get { return borderSize; }
            set { borderSize = value; }
        }


        #endregion


        /// <summary>
        /// Проверка дали точка point принадлежи на елемента.
        /// </summary>
        /// <param name="point">Точка</param>
        /// <returns>Връща true, ако точката принадлежи на елемента и
        /// false, ако не пренадлежи</returns>
        public virtual bool ContainsPoint(PointF point)
        {
            return Rectangle.Contains(point.X, point.Y);
        }

        /// <summary>
        /// Визуализира елемента.
        /// </summary>
        /// <param name="grfx">Къде да бъде визуализиран елемента.</param>
        public virtual void DrawSelf(Graphics grfx) { }

        public virtual void Move(float dx, float dy)
        {
            Location = new PointF(Location.X + dx, Location.Y + dy);
        }

        public virtual void RotateShape(Graphics grfx, PointF? point = null)
        {
            if (point != null)
            {
                float distanceToCenterX = point.Value.X - Location.X;
                float distanceToCenterY = point.Value.Y - Location.Y;
                grfx.TranslateTransform(Location.X + distanceToCenterX, Location.Y + distanceToCenterY);
                grfx.RotateTransform(CurrentAngle);
                grfx.TranslateTransform(-(Location.X + distanceToCenterX), -(Location.Y + distanceToCenterY));
                if (CurrentAngle == 360) currentAngle = 0;

            }
            else
            {
                grfx.TranslateTransform(Location.X + Width / 2, Location.Y + Height / 2);
                grfx.RotateTransform(CurrentAngle);
                grfx.TranslateTransform(-(Location.X + Width / 2), -(Location.Y + Height / 2));
                if (CurrentAngle == 360) currentAngle = 0;
            }
        }

        public virtual void ZoomInShape()
        {
            Location = new PointF(Location.X - 10, Location.Y - 10);
            Width += 20;
            Height += 20;
        }

        public virtual void ZoomOutShape()
        {
            Location = new PointF(Location.X + 10, Location.Y + 10);
            Width -= 20;
            Height -= 20;
        }
    }
}
