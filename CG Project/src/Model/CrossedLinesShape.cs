﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Draw.src.Model
{
    [Serializable]
    public class CrossedLinesShape : Shape
    {
        #region Constructor

        public CrossedLinesShape(RectangleF rect) : base(rect)
        {
        }

        public CrossedLinesShape(RectangleShape rectangle) : base(rectangle)
        {
        }

        #endregion

        /// <summary>
        /// Проверка за принадлежност на точка point към правоъгълника.
        /// В случая на правоъгълник този метод може да не бъде пренаписван, защото
        /// Реализацията съвпада с тази на абстрактния клас Shape, който проверява
        /// дали точката е в обхващащия правоъгълник на елемента (а той съвпада с
        /// елемента в този случай).
        /// </summary>
        public override bool ContainsPoint(PointF point)
        {
            if (base.ContainsPoint(point))
                // Проверка дали е в обекта само, ако точката е в обхващащия правоъгълник.
                // В случая на правоъгълник - директно връщаме true
                return true;
            else
                // Ако не е в обхващащия правоъгълник, то неможе да е в обекта и => false
                return false;
        }

        /// <summary>
        /// Частта, визуализираща конкретния примитив.
        /// </summary>
        public override void DrawSelf(Graphics grfx)
        {
            PointF[] first = { new PointF(Rectangle.X, Rectangle.Y), new PointF(Rectangle.X + Rectangle.Width, Rectangle.Y + Rectangle.Height) };
            PointF[] second = { new PointF(Rectangle.X+Rectangle.Width, Rectangle.Y), new PointF(Rectangle.X, Rectangle.Y + Rectangle.Height) };
            
            base.RotateShape(grfx);
            //grfx.FillRectangle(new SolidBrush(FillColor), Rectangle.X, Rectangle.Y, Rectangle.Width, Rectangle.Height);
            grfx.DrawLines(new Pen(BorderColor, BorderSize),first);
            grfx.DrawLines(new Pen(BorderColor, BorderSize), second);
            grfx.ResetTransform();
        }
    }
}
