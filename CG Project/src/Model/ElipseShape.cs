﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Draw.src.Model
{
    [Serializable]
    class ElipseShape : Shape
    {
        #region Constructor

        public ElipseShape()
        {
        }
        public ElipseShape(RectangleF rect)
            : base(rect)
        {
        }

        public ElipseShape(RectangleShape rectangle)
            : base(rectangle)
        {
        }

        #endregion

        /// <summary>
        /// Проверка за принадлежност на точка point към правоъгълника.
        /// В случая на правоъгълник този метод може да не бъде пренаписван, защото
        /// Реализацията съвпада с тази на абстрактния клас Shape, който проверява
        /// дали точката е в обхващащия правоъгълник на елемента (а той съвпада с
        /// елемента в този случай).
        /// </summary>
        public override bool ContainsPoint(PointF point)
        {
            double a = Width / 2;
            double b = Height / 2;
            double h = Location.X + a;
            double k = Location.Y + b;
            double result = ((Math.Pow((point.X - h), 2)) / Math.Pow(a, 2)) + (Math.Pow((point.Y - k), 2) / Math.Pow(b, 2)) - 1;
            return (result <= 0);
        }

        /// <summary>
        /// Частта, визуализираща конкретния примитив.
        /// </summary>
        public override void DrawSelf(Graphics grfx)
        {            
            base.DrawSelf(grfx);
            base.RotateShape(grfx);
            grfx.FillEllipse(new SolidBrush(FillColor), Rectangle.X, Rectangle.Y, Rectangle.Width, Rectangle.Height);
            grfx.DrawEllipse(new Pen(BorderColor, BorderSize), Rectangle.X, Rectangle.Y, Rectangle.Width, Rectangle.Height);
            grfx.ResetTransform();
        }
    }
}
